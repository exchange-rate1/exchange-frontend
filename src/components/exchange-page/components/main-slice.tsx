import { createSlice } from "@reduxjs/toolkit";
import { RequestStatus } from "../../../shared/types/request-status";
import { ExchangeRates } from "../types";
import {
  convertExchangeRatesRequest,
  getExchangeRatesRequest,
} from "./extra-actions";

type RequestState = {
  status: RequestStatus;
};

type State = {
  getExchangeRatesRequest: RequestState;
  convertExchangeRatesRequest: RequestState;
  convertedRate: number | null;
  exchangeRates: ExchangeRates;
};

const initialState: State = {
  getExchangeRatesRequest: {
    status: RequestStatus.Ideal,
  },
  convertExchangeRatesRequest: {
    status: RequestStatus.Ideal,
  },

  convertedRate: null,
  exchangeRates: { rates: {}, base: "", timeStamp: 0 },
};

const mainSlice = createSlice({
  name: "converter",
  initialState,
  reducers: {
    resetConvertExchangeRatesRequest: (state) => {
      state.convertExchangeRatesRequest.status = RequestStatus.Ideal;
    },
    resetConvertedRate: (state) => {
      state.convertedRate = null;
    },
  },
  extraReducers: (builder) => {
    // get exchange rate API request
    builder.addCase(getExchangeRatesRequest.pending, (state) => {
      state.getExchangeRatesRequest.status = RequestStatus.Pending;
    });
    builder.addCase(getExchangeRatesRequest.fulfilled, (state, action) => {
      state.getExchangeRatesRequest.status = RequestStatus.Succeeded;
      state.exchangeRates = action.payload.payload;
    });
    builder.addCase(getExchangeRatesRequest.rejected, (state, action) => {
      state.getExchangeRatesRequest.status = RequestStatus.Failed;
    });

    // get exchange rate converted API request
    builder.addCase(convertExchangeRatesRequest.pending, (state) => {
      state.convertExchangeRatesRequest.status = RequestStatus.Pending;
    });
    builder.addCase(convertExchangeRatesRequest.fulfilled, (state, action) => {
      state.convertExchangeRatesRequest.status = RequestStatus.Succeeded;
      state.convertedRate = action.payload.payload;
    });
    builder.addCase(convertExchangeRatesRequest.rejected, (state, action) => {
      state.convertExchangeRatesRequest.status = RequestStatus.Failed;
    });
  },
});

export const { resetConvertExchangeRatesRequest, resetConvertedRate } =
  mainSlice.actions;
export default mainSlice.reducer;
