import { ExchangeRates } from "../../../types";

export const filterData = (
  data: ExchangeRates,
  search: string
): ExchangeRates => {
  const newExchangeRates: ExchangeRates = {
    base: data.base,
    timeStamp: data.timeStamp,
    rates: {},
  };
  Object.keys(data?.rates).forEach((countryCode, index) => {
    if (countryCode.includes(search)) {
      newExchangeRates.rates[countryCode] = data.rates[countryCode];
    }
  });
  return newExchangeRates;
};
