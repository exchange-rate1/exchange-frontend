export type ExchangeRates = {
  base: string;
  timeStamp: number;
  rates: { [key: string]: number };
};
