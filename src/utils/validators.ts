export function isAlpha(str: string) {
  return /^[a-zA-Z()]+$/.test(str);
}
