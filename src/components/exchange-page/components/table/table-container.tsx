import { connect, ConnectedProps } from "react-redux";
import { RootState } from "../../../../store/root-reducer";
import { getExchangeRatesRequest } from "../extra-actions";
import Table from "./table";

const mapStateToProps = (state: RootState) => ({
  getExchangeRatesRequestStatus: state.exchange.getExchangeRatesRequest.status,
  exchangeRates: state.exchange.exchangeRates,
});

const mapDispatchToProps = {
  sendGetExchangeRatesRequest: (date?: string) => getExchangeRatesRequest(date),
};

const connector = connect(mapStateToProps, mapDispatchToProps);

export type IProps = ConnectedProps<typeof connector>;

export default connector(Table);
