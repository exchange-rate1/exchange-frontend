import React from "react";
import Convertor from "./converter";
import Table from "./table";

const Home = () => {
  return (
    <div className="home">
      <div className="display-3 mb-5 home__heading">
        Welcome To Currency Exchange Rates Service
      </div>
      <br />
      <Convertor />
      <Table />
    </div>
  );
};

export default Home;
