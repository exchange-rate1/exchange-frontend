import axios from "axios";
import toast from "react-hot-toast";

export type ResponseErrorWithHandled = ResponseError & { isHandled: boolean };

export type ResponseError = {
  error: boolean;
  type: ExceptionTypes;
  code?: number;
  message?: string;
  messages?: string[];
};

export enum ExceptionTypes {
  System = "system",
  Validation = "validation",
  General = "general",
}
const instance = axios.create({
  baseURL: process.env.REACT_APP_API_BASE_URL,
});

instance.interceptors.response.use(
  (response) => response.data,
  (err): Promise<ResponseErrorWithHandled> => {
    let error: ResponseErrorWithHandled;
    console.log(process.env.REACT_APP_API_BASE_URL);
    if (isCustomError(err)) {
      error = { ...err.response.data, isHandled: false };

      switch (error.type) {
        case ExceptionTypes.System:
          toast.error(err.message);
          error.isHandled = true;
          break;

        case ExceptionTypes.Validation: {
          toast.error(err.message);
          error.isHandled = true;
          break;
        }
      }
    } else {
      error = {
        error: true,
        type: ExceptionTypes.System,
        message: err.message,
        isHandled: true,
      };
    }

    return Promise.reject(error);
  }
);

const isCustomError = (err: any): boolean => !!err.response?.data?.type;

export default instance;
