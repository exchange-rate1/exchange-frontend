import api from "../../../../api";

export type ResponseSuccess = {
  message?: string;
  payload?: any;
};

export type ConvertedRatesDto = {
  from: string;
  to: string;
  amount: number | undefined | null;
};

export const getExchangeRates = async (payload?: any) =>
  api.get<ResponseSuccess>("exrates", { params: { date: payload } });

export const getConvertedRates = async (payload: ConvertedRatesDto) => {
  return api.get<ResponseSuccess>("convert", { params: payload });
};
