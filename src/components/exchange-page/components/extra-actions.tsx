import { createAsyncThunk } from "@reduxjs/toolkit";
import { ResponseErrorWithHandled } from "../../../api";
import {
  ConvertedRatesDto,
  getExchangeRates,
  ResponseSuccess,
  getConvertedRates,
} from "./helpers/converter-api";

export const getExchangeRatesRequest = createAsyncThunk<
  ResponseSuccess,
  string | undefined,
  { rejectValue: ResponseErrorWithHandled }
>("exchange/get", async (args, thunkAPI) => {
  try {
    return (await getExchangeRates(args)) as ResponseSuccess;
  } catch (err) {
    return thunkAPI.rejectWithValue(err);
  }
});

export const convertExchangeRatesRequest = createAsyncThunk<
  ResponseSuccess,
  ConvertedRatesDto,
  { rejectValue: ResponseErrorWithHandled }
>("exchange/convert", async (args, thunkAPI) => {
  try {
    return (await getConvertedRates(args)) as ResponseSuccess;
  } catch (err) {
    return thunkAPI.rejectWithValue(err);
  }
});
