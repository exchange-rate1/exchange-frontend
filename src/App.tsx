import React from "react";
import { Toaster } from "react-hot-toast";
import ExchangePage from "./components/exchange-page/components";

function App() {
  return (
    <div className="app">
      <Toaster />
      <ExchangePage />
    </div>
  );
}

export default App;
