# Description
This is the Frontend of Exchange Rates Service (link below). React with redux-toolkit has been used to create this repo. 

### Links to other related repositories

- Hosted on: [HerokuApp with AWS EC2 backend](https://exrate-app.herokuapp.com/)
- Repo Group: [exchange-rate1](https://gitlab.com/exchange-rate1)
- Frontend: [exchange-frontend](https://gitlab.com/exchange-rate1/exchange-frontend)
- Backend: [exchange-rate](https://gitlab.com/exchange-rate1/exchange-rate) 
- Microservices: [exchange-latest](https://gitlab.com/exchange-rate1/exchange-latest) [exchange-historical](https://gitlab.com/exchange-rate1/exchange-historical)



# deployed at https://exrate-app.herokuapp.com/

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:4000](http://localhost:4000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

