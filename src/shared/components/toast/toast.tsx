import React from "react";
import { useState } from "react";
import { Col, Row, Toast as BootstrapToast } from "react-bootstrap";

type ToastProps = { header: string; content: string };

const Toast: React.FC<ToastProps> = ({ header, content }) => {
  const [showA, setShowA] = useState(true);

  const toggleShowA = () => setShowA(!showA);

  return (
    <Row>
      <Col xs={6}>
        <BootstrapToast show={showA} onClose={toggleShowA}>
          <BootstrapToast.Header>{header}</BootstrapToast.Header>
          <BootstrapToast.Body>{content}</BootstrapToast.Body>
        </BootstrapToast>
      </Col>

      <Col xs={6} className="my-1"></Col>
    </Row>
  );
};

export default Toast;
