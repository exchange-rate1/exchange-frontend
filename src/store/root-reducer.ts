import { combineReducers } from "@reduxjs/toolkit";
import { mainReducer } from "../components/exchange-page/components";

export const rootReducer = combineReducers({
  exchange: mainReducer,
});
export type RootState = ReturnType<typeof rootReducer>;
