import { connect, ConnectedProps } from "react-redux";
import { RootState } from "../../../../store/root-reducer";
import { convertExchangeRatesRequest } from "../extra-actions";
import { ConvertedRatesDto } from "../helpers/converter-api";
import {
  resetConvertedRate,
  resetConvertExchangeRatesRequest,
} from "../main-slice";
import Convertor from "./converter";

const mapStateToProps = (state: RootState) => ({
  convertExchangeRatesRequestStatus:
    state.exchange.getExchangeRatesRequest.status,
  convertedRate: state.exchange.convertedRate,
});

const mapDispatchToProps = {
  sendConvertExchangeRateRequest: (payload: ConvertedRatesDto) =>
    convertExchangeRatesRequest(payload),
  resetConvertExchangeRateRequestStatus: () =>
    resetConvertExchangeRatesRequest(),
  resetConvertedExchangeRate: () => resetConvertedRate(),
};

const connector = connect(mapStateToProps, mapDispatchToProps);

export type IProps = ConnectedProps<typeof connector>;

export default connector(Convertor);
