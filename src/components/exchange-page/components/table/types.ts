export type TransformedExchangeRate = {
  id: number;
  countryCode: string;
  rate: string;
};
