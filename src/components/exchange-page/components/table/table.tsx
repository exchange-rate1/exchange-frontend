import React, { useMemo, useState } from "react";
import { IProps } from "./table-container";
import BootstrapTable from "react-bootstrap-table-next";
import { useEffect } from "react";
import { Button, FormControl, InputGroup, Spinner } from "react-bootstrap";
import { RequestStatus } from "../../../../shared/types/request-status";
import { TransformedExchangeRate } from "./types";
import { filterData } from "./helpers/filter-data";
import TableSkeleton from "../../../../shared/components/skeletons";
const Table: React.FC<IProps> = ({
  sendGetExchangeRatesRequest,
  getExchangeRatesRequestStatus,
  exchangeRates,
}) => {
  // states.
  const [date, setDate] = useState("");
  const [search, setSearch] = useState<string>();
  const [filteredData, setFilteredData] = useState(exchangeRates);

  // useEffects.
  useEffect(() => {
    sendGetExchangeRatesRequest();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (getExchangeRatesRequestStatus === RequestStatus.Succeeded) {
      if (search) {
        const newExchangeRates = filterData(exchangeRates, search);
        setFilteredData(newExchangeRates);
      } else {
        setFilteredData(exchangeRates);
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getExchangeRatesRequestStatus]);
  // data mapping.
  const columns = [
    {
      dataField: "countryCode",
      text: "Country Code",
    },
    {
      dataField: "rate",
      text: "Rates",
    },
  ];

  // useCallback
  const data: TransformedExchangeRate[] = useMemo(() => {
    return Object.keys(filteredData?.rates).map((countryCode, index) => {
      return {
        id: index,
        countryCode,
        rate: (filteredData?.rates[countryCode]).toFixed(2),
      };
    });
  }, [filteredData]);

  // onChange handlers.
  const onDateChange = (e: any) => {
    setDate(e.target.value);
  };
  const onSearchChange = (e: any) => {
    setSearch(e.target.value);
  };

  // onClick handlers.
  const onApplyFilters = () => {
    if (date) {
      sendGetExchangeRatesRequest(date);
    }

    if (search) {
      setFilteredData(filterData(exchangeRates, search));
    } else {
      setFilteredData(exchangeRates);
    }
  };
  return (
    <>
      <div className="display-3 mt-5 heading-1">Exchange Rates List</div>
      <div className="filters d-flex justify-content-center align-items-center center ">
        <InputGroup className="search-bar ">
          <InputGroup.Prepend>
            <InputGroup.Text id="basic-addon1">
              <i className="fa fa-search"></i>
            </InputGroup.Text>
          </InputGroup.Prepend>
          <FormControl
            value={search}
            onChange={onSearchChange}
            placeholder="Search using country code"
            aria-label="Search using country code"
            aria-describedby="basic-addon1"
          />
        </InputGroup>
        <div className="datePicker ml-4 ">
          <FormControl type="date" onChange={(e) => onDateChange(e)} />
          <Button
            className="btn btn-solid ml-3"
            onClick={() => onApplyFilters()}
          >
            Apply Filters
          </Button>
        </div>
        {getExchangeRatesRequestStatus === RequestStatus.Pending ? (
          <Spinner className="ml-2" animation="border" role="status">
            <span className="sr-only">Loading...</span>
          </Spinner>
        ) : null}
      </div>

      <div className="table">
        {getExchangeRatesRequestStatus === RequestStatus.Pending ? (
          <TableSkeleton colCount={2} rowCount={10} />
        ) : (
          <BootstrapTable
            data={data}
            columns={columns}
            keyField={"id"}
          ></BootstrapTable>
        )}
      </div>
    </>
  );
};

export default Table;
