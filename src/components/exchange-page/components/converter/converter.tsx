import React, { useEffect, useState } from "react";
import { Alert, Button, Form } from "react-bootstrap";
import toast from "react-hot-toast";
import { RequestStatus } from "../../../../shared/types/request-status";
import { isAlpha } from "../../../../utils";
import { IProps } from "./converter-container";

const Convertor: React.FC<IProps> = ({
  sendConvertExchangeRateRequest,
  convertExchangeRatesRequestStatus,
  resetConvertExchangeRateRequestStatus,
  resetConvertedExchangeRate,
  convertedRate,
}) => {
  // states.
  const [amount, setAmount] = useState<number>();
  const [from, setFrom] = useState("");
  const [to, setTo] = useState("");
  const [errorObj, setErrorObj] = useState<{ error: boolean; message: string }>(
    {
      error: false,
      message: "",
    }
  );

  // useEffects
  useEffect(() => {
    if (convertExchangeRatesRequestStatus === RequestStatus.Succeeded) {
      resetConvertedExchangeRate();
      resetConvertExchangeRateRequestStatus();
    }

    if (convertExchangeRatesRequestStatus === RequestStatus.Failed) {
      toast.error("Something went wrong, please try again later");
    }
    // reset errorObj
    setErrorObj({ error: false, message: "" });
  }, [
    convertExchangeRatesRequestStatus,
    resetConvertExchangeRateRequestStatus,
    resetConvertedExchangeRate,
  ]);

  // change handlers.
  const onAmountChange = (e: any) => {
    const value = e.target.value ? e.target.value : null;
    setAmount(value);
  };
  function onFromChange(e: any) {
    setFrom(e.target.value);
  }
  const onToChange = (e: any) => {
    setTo(e.target.value);
  };

  // click handlers.
  const onConvertClick = () => {
    // reset errorObj
    setErrorObj({ error: false, message: "" });

    if (!amount) {
      setErrorObj({ error: true, message: "Please enter amount" });
    } else if (!from || from.length !== 3 || !isAlpha(from)) {
      setErrorObj({
        error: true,
        message: "Please enter valid country code in 'from' input",
      });
    } else if (!to || to.length !== 3 || !isAlpha(to)) {
      setErrorObj({
        error: true,
        message: "Please enter valid country code in 'to' input",
      });
    } else {
      sendConvertExchangeRateRequest({
        from: from.toLocaleUpperCase(),
        amount,
        to: to.toLocaleUpperCase(),
      });
    }
  };

  const onFlip = () => {
    setFrom(to);
    setTo(from);
  };
  // render.
  return (
    <div className="converter-box ">
      <div className="display-4 mb-5">Currency Converter</div>
      <div className="converter mb-5">
        <Form.Control
          className="converter__amount"
          type="number"
          placeholder="amount"
          value={amount}
          onChange={(e) => onAmountChange(e)}
        />
        <Form.Control
          className="converter__from"
          type="text"
          placeholder="from"
          value={from}
          onChange={(e) => onFromChange(e)}
        />
        <button className="flip" onClick={onFlip}></button>
        <Form.Control
          className="converter__to"
          type="text"
          placeholder="to"
          value={to}
          onChange={(e) => onToChange(e)}
        />
        <Button className="ml-5" onClick={onConvertClick}>
          Convert
        </Button>
      </div>
      {amount &&
      convertExchangeRatesRequestStatus !== RequestStatus.Ideal &&
      convertedRate ? (
        <h1 className="result mr-5">{convertedRate?.toFixed(2)}</h1>
      ) : null}

      {errorObj.error && (
        <Alert variant="danger" className="w-100 text-center">
          {errorObj.message}
        </Alert>
      )}
    </div>
  );
};

export default Convertor;
