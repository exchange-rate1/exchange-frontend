export enum RequestStatus {
  Ideal = "ideal",
  Pending = "pending",
  Succeeded = "succeeded",
  Failed = "failed",
}
